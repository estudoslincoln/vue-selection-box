import Vue from 'vue'
import Router from 'vue-router'
import VueRouter from 'vue-router'
import SelectionBoxView from './views/SelectionBoxView.vue'
import TesteView from './views/TesteView.vue'
import NotFoundView from './views/NotFoundView.vue'

Vue.use(VueRouter)
let App = Vue.extend({
	data() {
		return {
			fromView:''
		}
	}
})

// Create a router instance.
// You can pass in additional options here, but let's
// keep it simple for now.
let router = new VueRouter({
	history: false,
	linkActiveClass: 'active'
})

router.map({
    '*': {
        component: NotFoundView
    },
    '/teste': {
        component: TesteView
    },
    '/selection-box': {
        component: SelectionBoxView
    }    
})



router.start(App, '#app')