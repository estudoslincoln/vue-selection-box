# vue-selection-box

Componente criado seguindo o screencast do site [VedCasts](http://www.vedcasts.com.br/series/casos-reais)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
